<?php
declare(strict_types=1);

namespace PorkChopSandwichesTests\CHBS;

use PorkChopSandwiches\CHBS\Words\IWordsSource;

/**
 * Class SingleWordSource
 *
 * @author Cam Morrow
 */
class SingleWordSource implements IWordsSource {

    /** @var string $word */
    private $word;

    public function __construct (string $word) {
        $this->word = $word;
    }

    public function getWords (): array {
        return [$this->word];
    }
}
