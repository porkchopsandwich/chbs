<?php
declare(strict_types=1);

namespace PorkChopSandwichesTests\CHBS;

use PHPUnit\Framework\TestCase;
use PorkChopSandwiches\CHBS\Config\PasswordGeneratorConfig;
use PorkChopSandwiches\CHBS\Exceptions\CHBSInvalidWordsSourceException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingConfigException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingWordsSourceException;
use PorkChopSandwiches\CHBS\PasswordGenerator;
use PorkChopSandwiches\CHBS\Words\SimpleWordsSource;

/**
 * Class PasswordGeneratorTest
 *
 * @author Cam Morrow
 */
final class PasswordGeneratorTest extends TestCase {

    /**
     * @covers PasswordGenerator::setConfig
     * @covers PasswordGenerator::getConfig
     */
    public function testSetAndGetConfig () {
        $pg = new PasswordGenerator();
        $config = new PasswordGeneratorConfig();
        $pg->setConfig($config);
        self::assertSame($config, $pg->getConfig());
    }

    /**
     * @covers PasswordGenerator::setWordsSource
     * @covers PasswordGenerator::getWordsSource
     */
    public function testSetAndGetWordsSource () {
        $pg = new PasswordGenerator();
        $words_source = new SimpleWordsSource();
        $pg->setWordsSource($words_source);
        self::assertSame($words_source, $pg->getWordsSource());
    }

    /**
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSInvalidWordsSourceException
     */
    public function testMissingSourceThrowsException () {
        self::expectException(CHBSMissingWordsSourceException::class);
        $pg = new PasswordGenerator();
        $pg->setConfig(new PasswordGeneratorConfig());
        $pg->generate();
    }

    /**
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function testMissingConfigThrowsException () {
        self::expectException(CHBSMissingConfigException::class);
        $pg = new PasswordGenerator();
        $pg->setWordsSource(new SimpleWordsSource());
        $pg->generate();
    }

    /**
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function testMinimumLength () {
        $config = new PasswordGeneratorConfig();
        $pg = new PasswordGenerator(new SimpleWordsSource(), $config);

        $config->setMinimumLength(5);
        $config->setMinimumWords(2);
        for ($i = 0; $i < 50; $i++) {
            self::assertGreaterThanOrEqual(5, strlen($pg->generate()));
        }

        $config->setMinimumLength(50);
        $config->setMinimumWords(5);
        for ($i = 0; $i < 50; $i++) {
            self::assertGreaterThanOrEqual(50, strlen($pg->generate()));
        }
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testMinimumWords () {
        $config = new PasswordGeneratorConfig();
        $config->setSeparators([]);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        for ($i = 1; $i < 11; $i++) {
            $config->setMinimumWords($i);
            $expected = str_repeat($word, $i);
            self::assertEquals($expected, $pg->generate());
        }
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testUCFirst () {
        $repeat = 3;
        $config = new PasswordGeneratorConfig();
        $config->setSeparators([]);
        $config->setUCFirst(true);
        $config->setMinimumWords($repeat);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        $expected = str_repeat(ucfirst($word), $repeat);
        self::assertEquals($expected, $pg->generate());
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testUpperCase () {
        $repeat = 5;
        $config = new PasswordGeneratorConfig();
        $config->setSeparators([]);
        $config->setUpperCase(true);
        $config->setMinimumWords($repeat);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        $expected = str_repeat(strtoupper($word), $repeat);
        self::assertEquals($expected, $pg->generate());
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testPrependNumber () {
        $config = new PasswordGeneratorConfig();
        $config->setSeparators(["#"]);
        $config->setPrependANumber(true);
        $config->setAppendANumber(false);
        $config->setMinimumWords(1);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        $result = $pg->generate();
        self::assertEquals(1, preg_match("/^\d\#(" . $word . ")$/", $result), sprintf("Pattern did not match result '%s'", $result));
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testAppendNumber () {
        $config = new PasswordGeneratorConfig();
        $config->setSeparators(["!"]);
        $config->setPrependANumber(false);
        $config->setAppendANumber(true);
        $config->setMinimumWords(1);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        $result = $pg->generate();
        self::assertEquals(1, preg_match("/^(" . $word . ")\!\d$/", $result), sprintf("Pattern did not match result '%s'", $result));
    }

    /**
     * @throws CHBSInvalidWordsSourceException
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     */
    public function testPrependAndAppendNumber () {
        $config = new PasswordGeneratorConfig();
        $config->setSeparators(["%"]);
        $config->setPrependANumber(true);
        $config->setAppendANumber(true);
        $config->setMinimumWords(1);
        $word = "porkchop";
        $words = new SingleWordSource($word);
        $pg = new PasswordGenerator($words, $config);
        $result = $pg->generate();
        self::assertEquals(1, preg_match("/^\d\%(" . $word . ")\%\d$/", $result), sprintf("Pattern did not match result '%s'", $result));
    }
}
