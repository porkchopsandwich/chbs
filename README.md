# Correct Horse Battery Staple
Mini-library for generating CHBS passwords.

## Usage
```php
# 1. Provide IWordSource instance... 

# 2. Configure...
$config = new PorkChopSandwiches\CHBS\Config\PasswordGeneratorConfig();
$config->setMinimumWords(3);

# 3. Create Password Generator instance...
$pg = new PorkChopSandwiches\CHBS\PasswordGenerator($word_source, $config);

# Generate a single password...
$single_password = $pg->generate();

# Generate many passwords...
foreach ($pg->generator() as $password) {
  # ...
}
```

## Tests
`./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/`
