<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Exceptions;

/**
 * Class CHBSMissingConfigException
 *
 * @author Cam Morrow
 */
class CHBSMissingConfigException extends CHBSException {
    
}
