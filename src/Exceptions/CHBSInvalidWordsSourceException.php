<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Exceptions;

/**
 * Class CHBSInvalidWordsSourceException
 *
 * @author Cam Morrow
 */
class CHBSInvalidWordsSourceException extends CHBSException {
    
}
