<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Exceptions;

use Exception;

/**
 * Class CHBSException
 *
 * @author Cam Morrow
 */
abstract class CHBSException extends Exception {}
