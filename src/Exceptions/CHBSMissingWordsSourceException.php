<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Exceptions;

/**
 * Class CHBSMissingWordsSourceException
 *
 * @author Cam Morrow
 */
class CHBSMissingWordsSourceException extends CHBSException {
    
}
