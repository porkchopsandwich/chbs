<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS;

use Generator;
use PorkChopSandwiches\CHBS\Config\IPasswordGeneratorConfig;
use PorkChopSandwiches\CHBS\Exceptions\CHBSInvalidWordsSourceException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingConfigException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingWordsSourceException;
use PorkChopSandwiches\CHBS\Words\IWordsSource;

/**
 * Class PasswordGenerator
 *
 * Implementation of the IPasswordGenerator contract.
 *
 * @author Cam Morrow
 */
class PasswordGenerator implements IPasswordGenerator {

    /** @var IWordsSource|null $words_source */
    protected $words_source;
    /** @var IPasswordGeneratorConfig|null $config */
    protected $config;

    /**
     * PasswordGenerator constructor.
     *
     * @param IWordsSource|null             $words_source
     * @param IPasswordGeneratorConfig|null $config
     */
    public function __construct (IWordsSource $words_source = null, IPasswordGeneratorConfig $config = null) {
        $this->words_source = $words_source;
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function setConfig (IPasswordGeneratorConfig $config) {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function getConfig () {
        return $this->config;
    }

    /**
     * @return IPasswordGeneratorConfig
     * @throws CHBSMissingConfigException
     */
    protected function resolveConfig (): IPasswordGeneratorConfig {
        if (!$this->config instanceof IPasswordGeneratorConfig) {
            throw new CHBSMissingConfigException("No Config set.");
        }
        return $this->config;
    }

    /**
     * @inheritdoc
     */
    public function setWordsSource (IWordsSource $words_source) {
        $this->words_source = $words_source;
    }

    /**
     * @inheritdoc
     */
    public function getWordsSource () {
        return $this->words_source;
    }

    /**
     * @return IWordsSource
     * @throws CHBSMissingWordsSourceException
     */
    protected function resolveWordsSource (): IWordsSource {
        if (!$this->words_source instanceof IWordsSource) {
            throw new CHBSMissingWordsSourceException("No Words Source set.");
        }

        return $this->words_source;
    }

    /**
     * @return string
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function generate (): string {
        return $this->createPassword();
    }

    /**
     * @return Generator
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function generator (): Generator {
        while (true) {
            yield $this->generate();
        }
    }

    /**
     * @return string
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    protected function getRandomWord (): string {
        $words = $this->resolveWordsSource()->getWords();
        $word_count = count($words);

        if ($word_count === 0) {
            throw new CHBSInvalidWordsSourceException("Word Source has no words.");
        } else if ($word_count === 1) {
            return reset($words);
        } else {
            $random = rand(0, $word_count - 1);
            return $words[$random];
        }
    }

    /**
     * @param int $length
     *
     * @return array
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    protected function getRandomWords (int $length = 1): array {

        $words = [];

        while (count($words) < $length) {
            $words[] = $this->getRandomWord();
        }

        return $words;
    }

    protected function prepareWords (array $words): array {
        return array_map(function (string $word): string {
            return $this->prepareWord($word);
        }, $words);
    }

    /**
     * @param string $word
     *
     * @return string
     * @throws CHBSMissingConfigException
     */
    protected function prepareWord (string $word): string {
        $config = $this->resolveConfig();
        if ($config->getUCFirst()) {
            return ucfirst($word);
        } else if ($config->getUpperCase()) {
            return strtoupper($word);
        }
        return $word;
    }

    /**
     * @return string
     * @throws CHBSMissingConfigException
     */
    protected function getRandomSeparator (): string {
        $separators = $this->resolveConfig()->getSeparators();
        return count($separators) ? $separators[array_rand($separators)] : "";
    }

    /**
     * @param string $preselected_separator
     *
     * @return string
     * @throws CHBSMissingConfigException
     */
    protected function getRandomOrPreselectedSeparator (string $preselected_separator): string {
        return $this->resolveConfig()->getUseDifferentSeparators() ? $this->getRandomSeparator() : $preselected_separator;
    }

    /**
     * @param int|null $word_count
     *
     * @return string
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    protected function createPassword (int $word_count = null): string {
        $word_count = is_null($word_count) ? $this->resolveConfig()->getMinimumWords() : $word_count;

        $words = $this->prepareWords($this->getRandomWords($word_count));
        $elements = [];
        $separator = $this->getRandomSeparator();

        foreach ($words as $index => $word) {
            $elements[] = $word;
            if ($index < count($words) - 1) {
                $elements[] = $this->getRandomOrPreselectedSeparator($separator);
            }
        }

        if ($this->resolveConfig()->getPrependANumber()) {
            array_unshift($elements, $this->getRandomOrPreselectedSeparator($separator));
            array_unshift($elements, rand(0, 9));
        }

        if ($this->resolveConfig()->getAppendANumber()) {
            $elements[] = $this->getRandomOrPreselectedSeparator($separator);
            $elements[] = rand(0, 9);
        }

        $final = implode("", $elements);

        if (strlen($final) < $this->resolveConfig()->getMinimumLength()) {
            return $this->createPassword($word_count + 1);
        }

        return $final;
    }
}
