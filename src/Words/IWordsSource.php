<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Words;

/**
 * Interface IWordsSource
 *
 * A contract for an object that provides a list of candidate words to be used in the password generator.
 *
 * @author Cam Morrow
 */
interface IWordsSource {

    /**
     * Get a list of the words that may be used in the password generation.
     *
     * @return string[]
     */
    public function getWords (): array;
}
