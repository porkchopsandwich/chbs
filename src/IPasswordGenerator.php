<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS;

use Generator;
use PorkChopSandwiches\CHBS\Config\IPasswordGeneratorConfig;
use PorkChopSandwiches\CHBS\Exceptions\CHBSInvalidWordsSourceException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingConfigException;
use PorkChopSandwiches\CHBS\Exceptions\CHBSMissingWordsSourceException;
use PorkChopSandwiches\CHBS\Words\IWordsSource;

/**
 * Interface IPasswordGenerator
 *
 * A contract to generate CHBS passwords.
 *
 * @author Cam Morrow
 */
interface IPasswordGenerator {

    /**
     * @param IPasswordGeneratorConfig $config
     *
     * @return void
     */
    public function setConfig (IPasswordGeneratorConfig $config);

    /**
     * @return IPasswordGeneratorConfig|null
     */
    public function getConfig ();

    /**
     * @param IWordsSource $words_source
     *
     * @return void
     */
    public function setWordsSource (IWordsSource $words_source);

    /**
     * @return IWordsSource|null
     */
    public function getWordsSource ();

    /**
     * Generate a password from the current Config and Word Source.
     *
     * @return string
     *
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function generate (): string;

    /**
     * Produce an infinite generator for passwords.
     *
     * @return Generator
     *
     * @throws CHBSMissingConfigException
     * @throws CHBSMissingWordsSourceException
     * @throws CHBSInvalidWordsSourceException
     */
    public function generator (): Generator;
}
