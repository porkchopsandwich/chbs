<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Config;

/**
 * Interface IPasswordGeneratorConfig
 *
 * Provides configuration for the Password Generator.
 *
 * @author Cam Morrow
 */
interface IPasswordGeneratorConfig {

    /**
     * Get the minimum number of words in the password.
     *
     * @return int
     */
    public function getMinimumWords (): int;

    /**
     * Get the minimum string length of the password.
     *
     * @return int
     */
    public function getMinimumLength (): int;

    /**
     * Get whether the password words should be Uppercase-First.
     *
     * @return bool
     */
    public function getUCFirst (): bool;

    /**
     * Get whether the password words should be UPPERCASE.
     *
     * @return bool
     */
    public function getUpperCase (): bool;

    /**
     * Get the list of available separators between words.
     * If the array is empty, no separator will be used.
     *
     * @return string[]
     */
    public function getSeparators (): array;

    /**
     * Get whether to append a number to the end of the password.
     *
     * @return bool
     */
    public function getAppendANumber (): bool;

    /**
     * Get whether to prepend a number to the beginning of the password.
     *
     * @return bool
     */
    public function getPrependANumber (): bool;

    /**
     * Get whether to use a different separator for each separation point in the password, or to reuse the same one.
     *
     * @return bool
     */
    public function getUseDifferentSeparators (): bool;
}
