<?php
declare(strict_types=1);

namespace PorkChopSandwiches\CHBS\Config;

/**
 * Class PasswordGeneratorConfig
 *
 * Sample concrete implementation of the IPasswordGeneratorConfig contract.
 *
 * @author Cam Morrow
 */
class PasswordGeneratorConfig implements IPasswordGeneratorConfig {

    private $minimum_words = 1;
    private $minimum_length = 8;
    private $uc_first = false;
    private $uppercase = false;
    private $separators = ["-"];
    private $append_a_number = false;
    private $prepend_a_number = false;
    private $use_different_separators = false;

    /**
     * @inheritdoc
     */
    public function getMinimumWords (): int {
        return $this->minimum_words;
    }

    /**
     * @param int $minimum_words
     *
     * @return void
     */
    public function setMinimumWords (int $minimum_words) {
        $this->minimum_words = $minimum_words;
    }

    /**
     * @inheritdoc
     */
    public function getMinimumLength (): int {
        return $this->minimum_length;
    }

    /**
     * @param int $minimum_length
     *
     * @return void
     */
    public function setMinimumLength (int $minimum_length) {
        $this->minimum_length = $minimum_length;
    }

    /**
     * @inheritdoc
     */
    public function getUCFirst (): bool {
        return $this->uc_first;
    }

    /**
     * @param bool $uc_first
     *
     * @return void
     */
    public function setUCFirst (bool $uc_first) {
        $this->uc_first = $uc_first;
    }

    /**
     * @inheritdoc
     */
    public function getUpperCase (): bool {
        return $this->uppercase;
    }

    /**
     * @param bool $uppercase
     *
     * @return void
     */
    public function setUpperCase (bool $uppercase) {
        $this->uppercase = $uppercase;
    }

    /**
     * @inheritdoc
     */
    public function getSeparators (): array {
        return $this->separators;
    }

    /**
     * @param string[] $separators
     *
     * @return void
     */
    public function setSeparators (array $separators) {
        $this->separators = $separators;
    }

    /**
     * @inheritdoc
     */
    public function getAppendANumber (): bool {
        return $this->append_a_number;
    }

    /**
     * @param bool $append_a_number
     *
     * @return void
     */
    public function setAppendANumber (bool $append_a_number) {
        $this->append_a_number = $append_a_number;
    }

    /**
     * @inheritdoc
     */
    public function getPrependANumber (): bool {
        return $this->prepend_a_number;
    }

    /**
     * @param bool $prepend_a_number
     *
     * @return void
     */
    public function setPrependANumber (bool $prepend_a_number) {
        $this->prepend_a_number = $prepend_a_number;
    }

    /**
     * @inheritdoc
     */
    public function getUseDifferentSeparators (): bool {
        return $this->use_different_separators;
    }

    /**
     * @param bool $use_different_separators
     *
     * @return void
     */
    public function setUseDifferentSeparators (bool $use_different_separators) {
        $this->use_different_separators = $use_different_separators;
    }
}
