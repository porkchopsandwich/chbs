<?php
declare(strict_types=1);

use PorkChopSandwich\CHBS\Config\PasswordGeneratorConfig;
use PorkChopSandwich\CHBS\PasswordGenerator;
use PorkChopSandwich\CHBS\Words\SimpleWordsSource;

require("vendor/autoload.php");

$source = new SimpleWordsSource;
$config = new PasswordGeneratorConfig;
$pg = new PasswordGenerator($source, $config);

foreach ($pg->generator() as $index => $password) {
    echo $password . "\n";
    if ($index > 30) {
        break;
    }
}
